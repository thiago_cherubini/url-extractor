# URL Extractor
-------------

### Pre-requisitos:

> - Java 8
> - AngularJS   
> - Maven 3
> - Spring Framework
> - PostgreSQL
> - Docker e Docker Compose

### Execução do sistema:

Compilar o projeto: 
```
mvn clean package docker:build
```
Executar o arquivo: 'docker-compose.yml' com o comando:
```
 docker-compose --file docker-compose.yml up
```
Para verificar se o sistema subiu corretamente, acessar o endereço: http://localhost:8080/index.html
