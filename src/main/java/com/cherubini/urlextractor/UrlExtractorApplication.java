package com.cherubini.urlextractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@SpringBootApplication
public class UrlExtractorApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlExtractorApplication.class, args);
    }
}
