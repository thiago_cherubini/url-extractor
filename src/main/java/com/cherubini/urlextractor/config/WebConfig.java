package com.cherubini.urlextractor.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@Configuration
public class WebConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {

            @Override
            public void contextInitialized(final ServletContextEvent paramServletContextEvent) {
                logger.info("URL Extractor - ServletContext initialized");
            }

            @Override
            public void contextDestroyed(final ServletContextEvent paramServletContextEvent) {
                logger.info("URL Extractor - ServletContext destroyed");
            }
        };
    }

}
