package com.cherubini.urlextractor.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * URL entity class
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@Getter
@Entity
@Table(name = "URL")
@AllArgsConstructor
public class URL {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "URL", length = 4000, nullable = false)
    private String url;

    URL() {
    }

    public URL(final String url) {
        this.url = url;
    }

}
