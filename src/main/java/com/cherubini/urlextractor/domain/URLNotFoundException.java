package com.cherubini.urlextractor.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class URLNotFoundException extends RuntimeException {

    public URLNotFoundException(String message) {
        super(message);
    }

}