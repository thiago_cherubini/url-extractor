package com.cherubini.urlextractor.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Facade for {@link URL}
 *
 * @author Cherubini
 * @version 1.0 22/09/18
 */
@Component
@AllArgsConstructor
public class URLFacade {

    private final URLService urlService;

    public List<URL> save(final URL url) {
        return this.urlService.save(url);
    }

    public List<URL> delete(final Long id) {
        return this.urlService.delete(id);
    }

    public List<URL> getAll() {
        return this.urlService.getAll();
    }

}
