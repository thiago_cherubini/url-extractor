package com.cherubini.urlextractor.domain;

import com.cherubini.urlextractor.infra.DefaultURLRetriever;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@Service
@AllArgsConstructor
public class URLService {

    private final URLRepository repository;

    private final DefaultURLRetriever defaultURLRetriever;

    public List<URL> save(final URL url) {
        this.repository.deleteAll();
        final Set<URL> urls = new HashSet<>();
        urls.add(url);
        urls.addAll(this.defaultURLRetriever.retrieveLinks(url.getUrl()));
        return this.repository.saveAll(urls);
    }

    public List<URL> delete(final Long id) {
        repository.findById(id).ifPresent(repository::delete);
        return getAll();
    }

    public List<URL> getAll() {
        return repository.findAll();
    }

}
