package com.cherubini.urlextractor.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
public interface URLRepository extends JpaRepository<URL, Long> {

}
