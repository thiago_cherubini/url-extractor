package com.cherubini.urlextractor.restapi;

import com.cherubini.urlextractor.domain.URL;
import com.cherubini.urlextractor.domain.URLFacade;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@RestController
@RequestMapping(value = "/urls")
@AllArgsConstructor
public class URLController {

    private final URLFacade urlFacade;

    @PostMapping
    public List<URL> save(@RequestBody final URL url) {
        return urlFacade.save(url);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<URL> urls() {
        return urlFacade.getAll();
    }

    @DeleteMapping(value = "/{id}")
    public List<URL> delete(@PathVariable("id") final Long id) {
        return urlFacade.delete(id);
    }

}
