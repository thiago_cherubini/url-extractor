package com.cherubini.urlextractor.infra;

import com.cherubini.urlextractor.domain.URL;
import liquibase.util.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toSet;

/**
 * Class comments goes here...
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@Component
@AllArgsConstructor
public class DefaultURLRetriever {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final Pattern URL_PATTERN = Pattern.compile(
            "((ht|f)tp(s?):\\/\\/|www\\.)" + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    private RestTemplate restTemplate;

    public Set<URL> retrieveLinks(final String site) {
        Set<URL> links = new HashSet<>();
        final Set<URL> initialLinks = getLinksFromURL(getURL(site)).stream().map(url -> new URL(url)).collect(toSet());
        if (!initialLinks.isEmpty()) {
            links = initialLinks.stream()
                    .flatMap(url -> getLinksFromURL(getURL(url.getUrl()))
                            .stream()
                            .map(link -> new URL(link)))
                    .collect(toSet());
        }
        return links.isEmpty()? initialLinks : links;

    }

    private String getURL(final String site) {
        try{
            return restTemplate.getForObject(site, String.class, 200);
        }catch (final Exception e){ //We're catching Exception because we can't know what kind of exception will be thrown.
            logger.info(e.getMessage());
            return site;
        }
    }

    private Set<String> getLinksFromURL(final String message) {
        final Set<String> containedUrls = new HashSet<>();
        try{
            final Matcher matcher = URL_PATTERN.matcher(message);
            while (matcher.find()) {
                final String link = message.substring(matcher.start(0), matcher.end(0));
                if(!StringUtils.isEmpty(link)) {
                    containedUrls.add(link);
                }
            }
            return containedUrls;
        }catch (final NullPointerException e){
            logger.info(e.getMessage());
            return containedUrls;
        }
    }
}
