angular.module('urlController', [])
	.controller('mainController', ['$scope','$http','Urls', function($scope, $http, Urls) {
		$scope.formData = {};
		$scope.loading = true;
		$scope.errorMessage = false;

		Urls.get()
		    .success(function(data) {
			    $scope.urls = data;
				$scope.loading = false;
		});

		$scope.createUrl = function() {
		    if ($scope.formData.url != undefined) {
				$scope.loading = true;

				Urls.create($scope.formData)
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {};
						$scope.urls = data;
				    }).error(function(data){
				        $scope.loading = false;
                        $scope.formData = {};
                        $scope.errorMessage = true;
                        $scope.errorMessage = data;
				    });
			}
		};

		$scope.deleteUrl = function(id) {
			$scope.loading = true;

			Urls.delete(id)
				.success(function(data) {
					$scope.loading = false;
					$scope.urls = data;
			});
		};
	}]);