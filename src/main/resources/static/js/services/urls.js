angular.module('urlService', [])
	.factory('Urls', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/urls');
			},
			create : function(urlData) {
				return $http.post('/urls', urlData);
			},
			delete : function(id) {
				return $http.delete('/urls/' + id);
			}
		}
	}]);