--liquibase formatted sql

--changeset cherubini:01
--comment: Create sequence
CREATE SEQUENCE  "URL_ID_SEQ"  INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
--rollback DROP SEQUENCE URL_ID_SEQ;

--changeset cherubini:02
--comment: Create table URL
CREATE TABLE URL (
    ID  SERIAL PRIMARY KEY,
    URL VARCHAR(4000) NOT NULL
);
--rollback drop table URL;

--changeset cherubini:03
--comment: Insert URL table
INSERT INTO URL (URL) VALUES ('https://www.wikipedia.org');
--rollback DELETE FROM URL WHERE URL IN ('https://www.wikipedia.org');
