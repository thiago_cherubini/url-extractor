package com.cherubini.urlextractor.config;

import net.minidev.json.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * Abstract class for all integration tests
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
public abstract class IntegrationTestInitializer {

    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext context;

    private String getFileContentAsString(final String filePath) throws Exception {
        final InputStream inputStream = getClass().getResourceAsStream(String.format("/%s", filePath));
        final Scanner scanner = new Scanner(inputStream, "UTF-8");
        scanner.useDelimiter("\\A");
        final String contentAsString = scanner.next();
        scanner.close();
        return contentAsString;
    }

    protected String getJsonAsString(final String path) throws UnsupportedEncodingException, FileNotFoundException {
        final InputStream inputStream = getClass().getResourceAsStream(String.format("/%s", path));
        if (inputStream == null) {
            throw new FileNotFoundException("File " + path + " not found. A file named " + path + " must be present "
                    + "in the src/test/resources folder of the project whose class matches being tested.");
        }
        return JSONValue.parse(new InputStreamReader(inputStream, "UTF-8")).toString();
    }

    protected String getContent(final MvcResult mvcResult) throws UnsupportedEncodingException {
        return mvcResult.getResponse().getContentAsString();
    }

}
