package com.cherubini.urlextractor.domain;

import com.cherubini.urlextractor.infra.DefaultURLRetriever;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link URLService}
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@RunWith(SpringRunner.class)
public class URLServiceTest {

    private URLService urlService;

    @MockBean
    private URLRepository repository;

    @MockBean
    private DefaultURLRetriever defaultURLRetriever;

    private final List<URL> urls = new ArrayList<>(2);

    private final Set<URL> urlsRetrieved = new HashSet<>(1);

    @Before
    public void setUp() {
        this.urlService = new URLService(repository, defaultURLRetriever);
        this.urls.add(new URL("https://jnjbrasil.com.br"));
        this.urls.add(new URL("https://www.wikipedia.org"));
        this.urlsRetrieved.add(new URL("https://www.wikipedia.org"));
    }

    @Test
    public void shouldSaveUrls() {
        given(this.repository.saveAll(any())).willReturn(this.urls);
        given(this.defaultURLRetriever.retrieveLinks("https://jnjbrasil.com.br")).willReturn(this.urlsRetrieved);
        assertThat(this.urlService.save(new URL("https://jnjbrasil.com.br"))).hasSize(2);
    }

    @Test
    public void shouldListAllUrls() {
        given(this.repository.findAll()).willReturn(this.urls);
        assertThat(this.urlService.getAll()).hasSize(2);
    }

    @Test
    public void shouldDeleteAnURL() {
        when(this.repository.findById(0L)).thenReturn(java.util.Optional.of(new URL()));
        this.urlService.delete(0l);
        verify(this.repository, times(1)).delete(any());
    }

}
