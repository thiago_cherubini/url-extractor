package com.cherubini.urlextractor.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link URLRepository}
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class URLRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private URLRepository repository;

    @Test
    public void findByIdShouldReturnAnURls() throws Exception {
        Optional<URL> url = this.repository.findById(1L);
        assertThat(url.get().getUrl()).isEqualTo("https://www.wikipedia.org");
        assertThat(url.get().getId()).isEqualTo(1L);
    }

    @Test
    public void findALLShouldReturnAllURls() throws Exception {
        this.entityManager.persist(new URL("https://jnjbrasil.com.br"));
        final List<URL> urls = this.repository.findAll();
        assertThat(urls).hasSize(2);
        assertThat(urls.get(1).getUrl()).isEqualTo("https://jnjbrasil.com.br");
        assertThat(urls.get(1).getId()).isEqualTo(2L);
    }

    @Test
    public void shouldDelteAURL() throws Exception {
        this.repository.findById(1L).ifPresent(repository::delete);
        final List<URL> urls = this.repository.findAll();
        assertThat(urls).hasSize(0);
    }

}
