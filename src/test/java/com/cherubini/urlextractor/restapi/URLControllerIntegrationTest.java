package com.cherubini.urlextractor.restapi;

import com.cherubini.urlextractor.config.IntegrationTestInitializer;
import com.cherubini.urlextractor.domain.URL;
import com.cherubini.urlextractor.domain.URLService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * {@code @WebMvcTest} based tests for {@link URLController}
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class URLControllerIntegrationTest extends IntegrationTestInitializer {

    @Before
    public void prepare() throws Exception {
        mockMvc = webAppContextSetup(context).build();
    }

    @Test
    public void getAllShouldReturnUrls() throws Exception {
        final MvcResult mvcResult = this.mockMvc.perform(get("/urls")).andExpect(status().isOk()).andReturn();
        assertEquals(getJsonAsString("jsons/expected_urls.json"), getContent(mvcResult), true);
    }

    @Test
    public void shouldSaveTheURls() throws Exception {
        final String request = getJsonAsString("jsons/request_url.json");
        this.mockMvc.perform(post("/urls").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(request))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void shouldDeleteAnURL() throws Exception {
        this.mockMvc.perform(delete("/urls/1").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("1L"))
                .andExpect(status().isOk()).andReturn();
    }
}
