package com.cherubini.urlextractor.infra;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Tests for {@link DefaultURLRetriever}
 *
 * @author Cherubini
 * @version 1.0 23/09/18
 */
@RunWith(SpringRunner.class)
public class DefaultURLRetrieverTest {

    private DefaultURLRetriever defaultURLRetriever;

    @MockBean
    private RestTemplate restTemplate;

    private final String siteWithURls = "<!DOCTYPE html>" + "<html lang=\"pt-BR\"> " + "<head>" + "<meta charset=\"UTF-8\">"
            + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
            + "<link rel=\"profile\" href=\"http://www.gmpg.org/xfn/11\">"
            + "<link rel=\"pingback\" href=\"http://www.jnjbrasil.com.br/xmlrpc.php\">";

    private final String siteWithoutURls = "<!DOCTYPE html>" + "<html lang=\"pt-BR\"> " + "<head>" + "<meta charset=\"UTF-8\">"
            + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" + "<link rel=\"profile\""
            + "<link rel=\"pingback\"";

    @Before
    public void setUp() {
        this.defaultURLRetriever = new DefaultURLRetriever(restTemplate);
    }

    @Test
    public void shouldFindURLsOnTheSite() {
        given(this.restTemplate.getForObject("site", String.class, 200)).willReturn(siteWithURls);
        assertThat(this.defaultURLRetriever.retrieveLinks("site")).hasSize(2);
    }

    @Test
    public void shouldNotFindURLsOnTheSite() {
        given(this.restTemplate.getForObject("site", String.class, 200)).willReturn(siteWithoutURls);
        assertThat(this.defaultURLRetriever.retrieveLinks("site")).hasSize(0);
    }

}
